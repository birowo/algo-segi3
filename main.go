package main

func segi3(max int) string {
	x := (float32(max) / 2) * (float32(max) + 1) //deret hitung
	slc := make([]byte, int(x)*max+max)
	l := 0
	for i := max - 1; i > -1; i-- {
		for j := i; j < max; j++ {
			k := 0
			for k < j {
				slc[l] = ' '
				k++
				l++
			}
			for k < max {
				slc[l] = '*'
				k++
				l++
			}
		}
		slc[l] = '\n'
		l++
	}
	return string(slc)
}
func main() {
	println(segi3(9))
}
